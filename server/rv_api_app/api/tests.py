from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from django.urls import reverse
from .models import VoteTable
# Create your tests here.


class TestVote(TestCase):
    def setUp(self):
        self.voteEntry = VoteTable(prompt_id=1, vote=True, camera_pos=1.0, camera_rot=1.0, state=2,
                                   comment='Sample Comment')

    # Test to check if new vote entry can be made
    def test_can_vote(self):
        old_vote_count = VoteTable.objects.count()
        self.voteEntry.save()
        new_vote_count = VoteTable.objects.count()
        self.assertNotEqual(old_vote_count, new_vote_count)


class ViewTestCase(TestCase):

    # Test to check if api responds with vote info
    def setUp(self):
        self.client = APIClient()
        self.vote_data = {'prompt_id': 1,
                          'vote': True,
                          'camera_pos': 0.0,
                          'camera_rot': 0.0,
                          'state': 1,
                          'comment': 'Sample Comment'}
        self.response = self.client.post(reverse('create'),
                                         self.vote_data,
                                         format='json')

    def test_can_api_create_vote(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_can_api_retrieve_vote(self):
        voteEntry = VoteTable.objects.get()
        response = self.client.get(
            reverse('details', kwargs={'pk': VoteTable.id}),
            format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, voteEntry)


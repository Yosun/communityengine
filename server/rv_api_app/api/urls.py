from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import CreateView, DetailsView, AllDetailsView, GetVotesByState

urlpatterns = {
    url(r'^vote/$', CreateView.as_view(), name='create'),
    url(r'^vote/(?P<pk>[0-9]+)/$', DetailsView.as_view(), name='details'),
    url(r'^vote/all/$', AllDetailsView.as_view(), name='all_details'),
    url(r'^vote/state_dict/$', GetVotesByState.as_view(), name='state_dict'),
}

urlpatterns = format_suffix_patterns(urlpatterns)

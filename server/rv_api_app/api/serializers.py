from rest_framework import serializers
from .models import VoteTable

class VoteTableSerializer(serializers.ModelSerializer):
    class Meta:
        model = VoteTable
        fields = ('id',
                  'prompt_id',
                  'vote',
                  'camera_pos',
                  'camera_rot',
                  'state',
                  'comment',
                  'pinpoint',
                  'snapshot')


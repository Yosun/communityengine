from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response

# Create your views here.
from rest_framework import generics
from .serializers import VoteTableSerializer
from .models import VoteTable

class CreateView(generics.ListCreateAPIView):
    queryset = VoteTable.objects.all()
    serializer_class = VoteTableSerializer

    def perform_create(self, serializer):
        serializer.save()

class DetailsView(generics.RetrieveAPIView):
    queryset = VoteTable.objects.all()
    serializer_class = VoteTableSerializer

class AllDetailsView(APIView):
    def get(self, request):
        all_votes = VoteTable.objects.all()
        serializer = VoteTableSerializer(all_votes, many=True)
        return Response(serializer.data)

class GetVotesByState(APIView):
    def get(self, request):
        state_dict = dict()

        # get number of states
        num_of_states = 3
        for state_id in range(1, num_of_states):
            state_dict[state_id] = VoteTable.objects.all().filter(state=state_id).count()
        return Response(state_dict)
from django.db import models

# Create your models here.

class VoteTable(models.Model):
    prompt_id = models.IntegerField(blank=False, default=None)
    vote = models.BooleanField(blank=False, default=None)

    camera_pos = models.TextField(blank=True, default=None)

    camera_rot = models.TextField(blank=True, default=None)

    state = models.FloatField(blank=False, default=None)

    comment = models.TextField(max_length=250, blank=True, default=None)

    pinpoint = models.TextField(blank=True, default=None)

    snapshot = models.URLField(blank=True, default=True)

    def __str__(self):
        return "vote: {} at pos:{}, rot:{} on state: {} w/comment {}".format(self.vote,
                                                                             self.camera_pos,
                                                                             self.camera_rot,
                                                                             self.state,
                                                                             self.comment,
                                                                             self.pinpoint,
                                                                             self.snapshot)
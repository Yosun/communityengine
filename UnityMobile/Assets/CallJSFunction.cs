﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class CallJSFunction : MonoBehaviour {

    [DllImport("__Internal")]
    private static extern void call_f1();

    private void Start()
    {
        call_f1();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonPlay : MonoBehaviour {

public Sprite[] TitleAppStart;
public float animationSpeed;

	public IEnumerator nukeMethod()
	{
		//destroy all game objects
		//Reverse: for (int i=TitleAppStart.Length -1; i >= 0; i-=1)
		for (int i=0; i < TitleAppStart.Length; i++)
		{ 	
			GetComponent<Image>().sprite = TitleAppStart[i];
			yield return new WaitForSeconds(animationSpeed);
		}
	}

	// Use this for initialization
	public void Start () {
		StartCoroutine(nukeMethod());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

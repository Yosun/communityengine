﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateController : MonoBehaviour {

    public static List<EachState> listStates = new List<EachState>();
    public static Dictionary<string,EachState> theStates = new Dictionary<string, EachState>();
    public static Dictionary<EachState, int> theStatesToNumber = new Dictionary<EachState, int>();

    public static EachState currentState;
    public bool useAnimation = true;

    public Transform tStateParent;

    public GameObject goUI_TimelineSlider; float fLastVal;

    public AnnotateController ac;

    public Animation animation;

    public GameObject goText_LeftPanel;

    public void SelectTimeline(){
        float f = UI2.GetSliderValue(goUI_TimelineSlider);

        /* if(Mathf.Abs(fLastVal-f)>0.01f){

         }*/

        ac.SwitchToAR();

        PlayAnimationAtTime(f);

        fLastVal = f;

        HackLoadText(f);
    }

    void HackLoadText(float val){
        if(val<=.2f){
            UI2.AssignText(goText_LeftPanel, listStates[0].stateMessage);
        }else if (val <= .4f) {
            UI2.AssignText(goText_LeftPanel, listStates[1].stateMessage);
        }else if (val <= .6f) {
            UI2.AssignText(goText_LeftPanel, listStates[2].stateMessage);
        }else if (val <= .8f) {
            UI2.AssignText(goText_LeftPanel, listStates[3].stateMessage);
        }else{
            UI2.AssignText(goText_LeftPanel, listStates[4].stateMessage);
        }
    }

    void PlayAnimationAtTime(float f){ 

           animation["Entire"].time = f * animation["Entire"].length;
           animation["Entire"].speed = 0;
        animation.Play("Entire");
        
    }
 
    public void PlayAll(){
        animation["Entire"].speed = 1;
        animation.Play("Entire");
    }

	private void Awake() {
        EachState[] states = tStateParent.GetComponentsInChildren<EachState>();

        for (int i = 0; i < states.Length; i++){
            theStates.Add(states[i].key , states[i]);
            listStates.Add(states[i]);
            theStatesToNumber.Add(states[i],i);
        }
        currentState = states[0];
	} 

    public void SelectState(){
        GameObject g = UI2.GetCurrentTouched();

        string name = g.name;
        if(theStates.ContainsKey(name)&&currentState!=theStates[name]){
            if(useAnimation) StartCoroutine( CalculateAnimationsToDesiredState(theStates[name]) );
            else {
                // just jump to show new state
                currentState.tInstantLoad.position = Mathf2.FarFarAway;
                currentState = theStates[name];
                currentState.tInstantLoad.localPosition = Vector3.zero;
            }
        }
    }

    IEnumerator CalculateAnimationsToDesiredState(EachState state){
        int n = theStatesToNumber[state];
        int n0 = theStatesToNumber[currentState];

        if (n > n0) {
            for (int i = n0; i <= n; i++) {
                AnimationStruct anistate = listStates[i].animationStruct[0]; 
                state.PlayAnimationState(0);
                yield return new WaitForSeconds(anistate.animationLength);
            }
        }else {
            for (int i = n0; i >= n; i--) {
                AnimationStruct anistate = listStates[i].animationStruct[1];
                state.PlayAnimationState(1);
                yield return new WaitForSeconds(anistate.animationLength);
            }
        }
        currentState = state;
    }

}

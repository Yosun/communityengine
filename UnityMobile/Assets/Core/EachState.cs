﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AnimationStruct{
    public string animationName;
    public float animationLength;
}

public class EachState : MonoBehaviour {

    public string key;

    public AnimationStruct[] animationStruct;

    public Transform tInstantLoad;

    [TextArea]
    public string stateMessage; 

    public void PlayAnimationState(int n){
        //TODO

    }
}

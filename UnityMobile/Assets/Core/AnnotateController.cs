﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AnnotateController : MonoBehaviour {

    public Transform tVR_Camera;
    public Transform tAR_Camera;

    public GameObject goAnnotationPrefab;

    static GameObject goCurrentAnnotation;

    static List<GameObject> theAnnotations = new List<GameObject>();

    public Transform tPanel_ShowSpecificCommentInfo;
        
    public string nameInputField = "Text";
    public string nameReaction = "Reaction";
    public string deleteButtonName = "DeleteButton";
    public Transform timlineSlider;

    string urlSnapshot = "http://www.nusoy.com/nusoy_round_cup.png";

    public Sprite[] texReaction; // 0 = positive; 1 = negative

    public static Vote currentVote;

    public APIHandler api;

    static bool annotationMode = false;

    public GameObject goHotspotParent;

    public GameObject goHideInVR;

    public void HotspotParentShow(bool f){
        goHotspotParent.SetActive(f);
    }

    public void SwitchToAR(){
        tAR_Camera.GetComponent<Camera>().enabled = true;
        tVR_Camera.GetComponent<Camera>().enabled = false;

        goHideInVR.SetActive(true);
    }
    public void SwitchToVR(){
        tVR_Camera.GetComponent<Camera>().enabled = true;
        tAR_Camera.GetComponent<Camera>().enabled = false;
        goHideInVR.SetActive(false);
    }
 
	private void Update() {
        if( ( Input.touchCount>0 && Input.GetTouch(0).phase == TouchPhase.Began )|| Input.GetMouseButtonUp(0) ){
            RaycastHit hit = Mathf2.WhatDidWeHit(Input.mousePosition);

            if (EventSystem.current.IsPointerOverGameObject(0)) return;
            if(hit.point!=Mathf2.FarFarAway && hit.transform!=null){
                print(hit.point);
                if (hit.transform.tag == "Annotation") {

                    SelectAnnotation(hit.transform);

                }else if (hit.transform.tag=="HotSpot"){
                    print("Hotspot " + hit.transform.name);
                    tVR_Camera.position = Mathf2.String2Vector2(hit.transform.name);

                    SwitchToVR();
                 

                } else {

                    if (annotationMode) { 
                        if (goCurrentAnnotation == null)
                            CreateAnnotation(hit.point+ new Vector3(0,5,0));
                        else {
                            goCurrentAnnotation.transform.position = hit.point+ new Vector3(0, 5, 0);
                        }
                    }

                }
            }

        }
	}

    public void EnableAnnotationMode(bool f) {
        if (f) goCurrentAnnotation = null;
        annotationMode = f;
    }
        

	void SelectAnnotation(Transform t){
        if(goCurrentAnnotation!=null)
        goCurrentAnnotation.transform.Find("Canvas Indicate Selected").GetComponent<Canvas>().enabled = false;


        goCurrentAnnotation = t.gameObject;

        //TODO show annotation selected
        goCurrentAnnotation.transform.Find("Canvas Indicate Selected").GetComponent<Canvas>().enabled=true;
    }

    public void DeleteAnnotation(){
        if(goCurrentAnnotation!=null){
            Destroy(gameObject.transform.parent);
        }
    }

	public GameObject CreateAnnotation(Vector3 v){
        goCurrentAnnotation = Instantiate(goAnnotationPrefab,v,Quaternion.identity);
        goCurrentAnnotation.transform.position = v;
        theAnnotations.Add(goCurrentAnnotation);
        UI2.Interactable(tPanel_ShowSpecificCommentInfo.Find(nameInputField), true);
        UI2.AssignInput(tPanel_ShowSpecificCommentInfo.Find(nameInputField),"");
        UI2.AssignText(tPanel_ShowSpecificCommentInfo.Find(nameInputField).Find("Placeholder").gameObject, "Enter your comment");
   
        tPanel_ShowSpecificCommentInfo.gameObject.SetActive(true);
        return goCurrentAnnotation;
    }
    public void CreateAnnotation(Vector3 v,string text,bool n,string snapshotURL){
        CreateAnnotation(v);
        UpdateText(text);
        UpdateReaction(n);
        tPanel_ShowSpecificCommentInfo.Find(deleteButtonName).gameObject.SetActive(false);
        UI2.Interactable( tPanel_ShowSpecificCommentInfo.Find(nameInputField), false);
        //TODO

    }

    public string GetText( ){
        return UI2.GetValueFromInput(tPanel_ShowSpecificCommentInfo.Find(nameInputField));
    }

    public bool GetReaction( ){
        int n = UI2.GetSliderValueInt(tPanel_ShowSpecificCommentInfo.Find(nameReaction));
        if (n == 0) return true;
        return false;
  
    }

    public void SubmitVote(){
        string reaction = GetReaction().ToString(); print(reaction);
        api.MakeVote("1", reaction , tVR_Camera.localPosition.ToString("F4"), tVR_Camera.localRotation.ToString("F4"), UI2.GetSliderValue(timlineSlider.gameObject), GetText(), transform.localPosition.ToString("F4"), urlSnapshot,UpdateURL);
        tPanel_ShowSpecificCommentInfo.gameObject.SetActive(false);
        EnableAnnotationMode(false);
        goShowAfterSubmit.SetActive(true);
        UI2.AssignText( goShowAfterSubmit.transform.Find("URL").gameObject,  "Uploading...");
    }
    public GameObject goShowAfterSubmit; static string lastURL;
    void UpdateURL(string u){
        
        MiniReturn mini = JsonUtility.FromJson<MiniReturn>(u);
        lastURL = "http://xrURL.xyz/?q=" + mini.id; print(lastURL);
        UI2.AssignText( goShowAfterSubmit.transform.Find("URL").gameObject,lastURL);
    }

    public void CopyURL(){
        CopyThang.CopyToClipboard(lastURL);
    }
    public void CloseShowAfter(){
        goShowAfterSubmit.SetActive(false);
    }
   

    public void UpdateText(string s){
        UI2.AssignText(goCurrentAnnotation.transform, nameInputField, s);
    }

    public void UpdateReaction(bool n){
        int b = 0;
        if (n) b = 1;
        UI2.AssignSprite(goCurrentAnnotation.transform, texReaction[b]);
    }

}
public class MiniReturn{
    public string id;
}
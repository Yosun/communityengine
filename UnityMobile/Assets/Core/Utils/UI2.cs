﻿// Jan 9 version includes rect
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public static class UI2 {

    [System.Serializable]
    public class RectContainer {
        public Vector3 pos;
        public Vector2 sizeDelta;
        public Vector3 anchoredPosition;
        public Vector2 anchorMin;
        public Vector2 anchorMax;
        public Vector2 pivot;
        public Vector3 rotation;
        public Vector3 scale;
        public RectTransform rtme;
    }

    public static void Interactable(Transform t,bool f){
        t.GetComponent<InputField>().interactable = f;
    }

    public static void SetRectTransform(RectTransform rt,RectContainer rc,bool setanchoredpos=true){
        // rt.sizeDelta = rc.sizeDelta;
        // rt.localPosition = Vector3.zero;
        Debug.Log("SetRectTransform "+rc.pos + " "+rc.sizeDelta);
        rt.anchorMin = rc.anchorMin;
        rt.anchorMax = rc.anchorMax;
        rt.pivot = rc.pivot;
        rt.localScale = rc.scale;
        if (setanchoredpos) {
            rt.anchoredPosition = rc.anchoredPosition;
            rt.position = rc.pos;
        }else{
            rt.anchoredPosition = rc.pos;
        }
        rt.localRotation = Quaternion.Euler( rc.rotation );
        rt.sizeDelta = rc.sizeDelta;
    }

    public static void MatchRT(RectTransform me,GameObject g){
        g.transform.parent = me.parent;
        g.transform.localScale = me.transform.localScale;
        g.GetComponent<RectTransform>().sizeDelta = me.sizeDelta;
        g.GetComponent<RectTransform>().rotation = me.rotation;
        g.GetComponent<RectTransform>().anchoredPosition3D = me.anchoredPosition3D;
    }

    public static void MatchRTRT(RectTransform match,RectTransform matchthis){
        match.anchorMax = matchthis.anchorMax;
        match.anchorMin = matchthis.anchorMin;
        match.pivot = matchthis.pivot;
        match.localScale = matchthis.localScale;
        match.anchoredPosition = matchthis.anchoredPosition;
        match.localRotation = matchthis.localRotation;
    }

    public static RectContainer GetRCfromRT(RectTransform rt){
        RectContainer rc = new RectContainer();
        rc.pos = rt.position;
        rc.anchoredPosition = rt.anchoredPosition;
        rc.sizeDelta = rt.sizeDelta;
        rc.anchorMin = rt.anchorMin;
        rc.anchorMax = rt.anchorMax;
        rc.pivot = rt.pivot;
        rc.rotation = rt.rotation.eulerAngles;
        rc.scale = rt.localScale;
        rc.rtme = rt;
        return rc;
    }

    public static Sprite CreateSprite(Texture2D tex) {
        return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero, 72f);
    }

    public static GameObject CreateButton(GameObject prefab, string childForSprite, Sprite s, bool resize) {
        GameObject g = GameObject.Instantiate(prefab);

        if (childForSprite.Length < 1) {
            g = AssignSprite(g, s);
        } else {
             AssignSprite(g.transform.Find(childForSprite), s);
        }

        if (resize) {
            Vector2 sd = g.GetComponent<RectTransform>().sizeDelta;
            Vector2 spritedims = new Vector2(s.texture.width, s.texture.height);
            float heighttowidth = spritedims.y / spritedims.x;
            sd = new Vector3(sd.x / heighttowidth, sd.y);
            g.GetComponent<RectTransform>().sizeDelta = sd;
        }

        return g;

    }
    public static GameObject CreateButton(GameObject prefab, string childForSprite, Texture2D tex, bool resize) {
        return CreateButton(prefab, childForSprite, CreateSprite(tex), resize);
    }
    public static GameObject AssignSprite(GameObject g, Sprite s) {
        g.GetComponent<Image>().sprite = s;
        return g;
    }
    public static GameObject AssignSprite(Transform t, Sprite s) {
        t.GetComponent<Image>().sprite = s;
        return t.gameObject;
    }

    public static void ToggleButton(Transform t, bool f) {
        t.GetComponent<Button>().interactable = f;
    }

    public static void ToggleButton(GameObject t, bool f) {
        t.GetComponent<Button>().interactable = f;
    }

    public static void ClearGOs(Transform t) {
        foreach (Transform child in t) {
            GameObject.Destroy(t.gameObject);
        }
    }
    public static void ClearGOs(Transform t, string omit) {
        foreach (Transform child in t) {
            if (child.name != omit)
                GameObject.Destroy(child.gameObject);
        }
    }
    public static GameObject CreateColorButton(GameObject prefab, Color color) {
        GameObject g = GameObject.Instantiate(prefab) as GameObject;
        g.GetComponent<Image>().color = color;
        return g;
    }

    public static GameObject GetCurrentTouched(){
        return EventSystem.current.currentSelectedGameObject;
    }
    public static string GetCurrentTouchedName(){
        return GetCurrentTouched().name;
    }

    public static void Reparent(Transform t,Transform newparent){
        t.parent = newparent;
    }

    public static void AssignText(GameObject game,string text){
        game.GetComponent<Text>().text = text;
    }

    public static void AssignText(Transform parent,string find,string text){
        if (find == "")
            parent.GetComponent<Text>().text = text;
        else 
        parent.Find(find).GetComponent<Text>().text = text;
    }

    public static void AssignSpriteToImage(Transform icon,Sprite sprite){
        icon.GetComponent<Image>().sprite = sprite;
    }

    public static void AssignSpriteToImage(GameObject icon, Texture2D tex) {
        icon.GetComponent<Image>().sprite = CreateSprite(tex);
    }

    public static bool CompareBoolImage(Transform image,Sprite sprite){
        if (image.GetComponent<Image>().sprite == sprite) return true;
        return false;
    }

    public static string GetValueFromInput(GameObject input){
        return input.GetComponent<InputField>().text; 
    }
    public static string GetValueFromInput(Transform input) {
        return input.GetComponent<InputField>().text;
    }

    public static void SizeGridAndButton(GameObject grid,GameObject prefab,Canvas canvas){
        float height = Screen.height;
        float dim = height * 0.5f; 
        grid.GetComponent<GridLayoutGroup>().cellSize =  1/canvas.scaleFactor * new Vector3(dim,dim,dim);
    }

    public static void AssignImageToFrom(GameObject to,GameObject from){
        to.GetComponent<Image>().sprite = from.GetComponent<Image>().sprite;
    }

    public static float GetSliderValue(GameObject g){
        return g.GetComponent<Slider>().value;
    }
    public static float GetSliderValue(Transform g) {
        return g.GetComponent<Slider>().value;
    }
    public static int GetSliderValueInt(Transform g) {
        return (int)g.GetComponent<Slider>().value;
    }
    public static bool GetToggleValue(Transform toggle){
        return toggle.GetComponent<Toggle>().isOn;
    }

    public static void AssignInput(Transform t,string s){
        t.GetComponent<InputField>().text = s;
    }

}

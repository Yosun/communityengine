﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

// GetAllVotes() : to get all the votes in json format

//  MakeVote(string prompt_id,
//            string vote,
//            string camera_pos,
//            string camera_rot,
//            string state,
//            string comment,
//            string pinpoint,
//            string snapshot) : To make a post request

// GetGetStateDict() : To get the state dictionary eg: {"1":3,"2":0} i.e state 1 has 3 votes, state 2 has 0

// GetVoteByID(String ID) : To get a vote by the primary key

//class for voting
[System.Serializable]
public class Vote
{
    //"prompt_id": null,
    //"vote": false,
    //"camera_pos": null,
    //"camera_rot": null,
    //"state": null,
    //"comment": ""

    public string prompt_id;
    public string vote;
    public string camera_pos;
    public string camera_rot;
    public float state;
    public string comment;
    public string pinpoint;
    public string snapshot;

    public string SaveToString()
    {
        return JsonUtility.ToJson(this);
    }
}

public class APIHandler : MonoBehaviour {

    //string server_url = "http://127.0.0.1:8000/";
    string server_url = "http://siddheshgupte1.pythonanywhere.com/";

    public delegate void WWWDataParseMethod(string s);
    public AnnotateController ac;

    private void Start()
    {
        //GetAllVotes();
        //MakeVote("0", "True", "2.0", "2.0", "0", "This Comm", "2.0", "https://gitlab.com/Yosun/communityengine/tree/master/UnityMobile");
        //GetStateDict();
        //GetVoteByID("1");
    }

    public void GetStateDict()
    {
        StartCoroutine(MakeGetReq("vote/state_dict/",ProcessResult));
    }

    public void GetAllVotes()
    {
        StartCoroutine(MakeGetReq("vote/",ProcessResult));
        Debug.Log("get all votes called");


    }

    void ProcessResult(string t){
        print(t);
        Vote[] v = JsonHelper.FromJson<Vote>(t);

        for (int i = 0; i < v.Length; i++){
            Vector3 pinpoint = Mathf2.String2Vector3(v[i].pinpoint);
            bool vote = Mathf2.String2Bool(v[i].vote);
            ac.CreateAnnotation(pinpoint,v[i].comment,vote,v[i].snapshot);
        }

    }

    public WWW MakeVote(string prompt_id,
                    string vote,
                    string camera_pos,
                    string camera_rot,
                    float state,
                    string comment,
                    string pinpoint,
                    string snapshot,WWWDataParseMethod fn)
    {
        string json_data = getVoteJson(prompt_id, vote, camera_pos, camera_rot, state, comment, pinpoint, snapshot);
        Debug.Log(json_data);

        Hashtable postHeader = new Hashtable();
        postHeader.Add("Content-Type", "application/json");

        var formData = System.Text.Encoding.UTF8.GetBytes(json_data);
        WWW www = new WWW(server_url + "vote/", formData, postHeader);
        StartCoroutine(WaitForRequest(www,fn));
        return www;
    }

    public void GetVoteByID(string ID,WWWDataParseMethod method)
    {
        StartCoroutine(MakeGetReq("vote/" + ID,method));
    }

    void ProcessSingle(string s){
        print("ProcessSingle "+s);
    } 
    public string getVoteJson(string prompt_id,
                    string vote,
                    string camera_pos,
                    string camera_rot,
                    float state,
                    string comment,
                    string pinpoint,
                    string snapshot)
    {
        Vote vote_ob = new Vote();
        vote_ob.prompt_id = prompt_id;
        vote_ob.vote = vote;
        vote_ob.camera_pos = camera_pos;
        vote_ob.camera_rot = camera_rot;
        vote_ob.state = state;
        vote_ob.comment = comment;
        vote_ob.pinpoint = pinpoint;
        vote_ob.snapshot = snapshot;

        //Debug.Log(vote_ob.SaveToString());
        return vote_ob.SaveToString();
    }

    IEnumerator MakeGetReq(string suffix,WWWDataParseMethod textout)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(server_url + suffix))
        {
            yield return www.Send();

            if(www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
               
                yield return www.downloadHandler;//.text;
                Debug.Log(www.downloadHandler.text);

                byte[] results = www.downloadHandler.data;

                textout(www.downloadHandler.text);
            }
        }
    }

    IEnumerator WaitForRequest(WWW data,WWWDataParseMethod fn)
    {
        yield return data; // Wait until the download is done
        if (data.error != null)
        {
            Debug.Log("There was an error sending request: " + data.error);
        }
        else
        {
            Debug.Log("WWW Request: " + data.text);
            fn(data.text);
        }
    }


}

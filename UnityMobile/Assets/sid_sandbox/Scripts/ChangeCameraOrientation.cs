﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeCameraOrientation : MonoBehaviour {

    public Camera ar_cam;
    APIHandler api;
    public GameObject fpscontroller;

    public Text test_txt;

    private void Awake()
    {
        api = GetComponent<APIHandler>();
        //ChangeOrientation("13");
    }
    
    public void ChangeOrientation(string id)
    {
        api.GetVoteByID(id,ProcessSingleIDText);

    }

    void ProcessSingleIDText(string t)
    {
        //test_txt.text = t;
        Vote v = JsonUtility.FromJson<Vote>(t);
        fpscontroller.transform.localPosition = Mathf2.String2Vector3(v.camera_pos);
        fpscontroller.transform.rotation = Mathf2.String2Quat(v.camera_rot);
        //ar_cam.transform.rotation = Mathf2.String2Quat(v.camera_rot);;
    }
}
